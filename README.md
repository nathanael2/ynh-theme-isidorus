# Theme for isidorus.fr
A simple theme for [Yunohost](https://yunohost.org), to customize my instance at [isidorus.fr](isidorus.fr).


## How to install

You can follow the instructions explained here:
https://yunohost.org/#/theming

## Screenshots
TODO: replace the screeshot :) 
![Yunohost-login](https://gitlab.com/stilobique/isidorus/-/wikis/uploads/d9d7ea8ed18aaf654b5fd026f21b32ba/Yunohost-login.jpg)

![Yunohost-login](https://gitlab.com/stilobique/isidorus/-/wikis/uploads/8e2f0a32d143b8a556d27fb1a2cfb533/Yunohost-Apps.jpg)